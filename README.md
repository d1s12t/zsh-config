## 说明
本配置配置文件通过stow进行管理，使用时需要将本仓库下载到家目录中,使用stow自动创建软链接到相应位置,zsh配置文件使用zinit框架对zsh插件进行管理
### 支持特性：
* 大部分插件懒加载以提高启动速度
* 代码提示（Ctrl+F应用提示）
* tab键模糊路径、命令、参数补全
* 兼容oh-my-zsh快捷键
* z命令快速跳转常用目录
* fif命令递归查找当前文件夹下的文件内容
* 上/下方向键查找符合子串匹配的历史命令
* Ctrl+T模糊查找目录并进入
* Ctrl+R模糊查找历史命令
* Ctrl+R模糊查找文件
* Ctrl+G模糊查找文件并使用nvim打开
* 命令权限不足执行失败时双击ESC快速为上一条命令添加sudo，或者快速为当前命令切换sudo


## 安装依赖
```bash
# arch系
sudo pacman -S zsh ripgrep subversion fzf zsh stow npm wqy-zenhei lua53
# debian系
apt install  zsh ripgrep subversion fzf zsh stow npm ttf-wqy-zenhei lua5.3
```
## 下载配置文件
```
git clone https://gitee.com/d1s125/zsh-config.git
```

## 安装zsh配置
```
bash install.sh
```
安装完毕后重新打开zsh会自动下载zinit以及zsh插件,由于速度较慢，推荐使用代理：
```
proxychains zsh
```

## 插件更新
```
zinit update --all
```
## zinit框架升级
```
zinit slef-update
```

## 移除zsh配置文件
```
cd zsh-config
stow -R zsh
```
## 所有快捷键
```bash
# 光标移动
bindkey "^A" beginning-of-line
bindkey "^E" end-of-line
bindkey "^F" forward-char
bindkey "^B" backward-char

# 删除单个字符
bindkey "^D" delete-char-or-list
bindkey "^H" backward-delete-char


# 其他快捷键
bindkey "^I" fzf-tab-complete
bindkey "^K" kill-line # 删除光标到结尾
bindkey "^L" clear-screen # 清屏
bindkey '^R' fzf-history-widget # fzf查找历史命令
bindkey "^W" fzf-find-widget # fzf查找文件
bindkey "^T" fzf-cd-widget # fzf查找进入文件夹
bindkey "^G" efif # 使用fzf查找文件内容并进行编辑
bindkey "^N" down-line-or-history # 上一条命令
bindkey "^P" up-line-or-history # 下一条命令
bindkey "^U" kill-whole-line # 删除整行
bindkey "^_" undo # <C-l> 撤销
bindkey "^Q" push-line
bindkey "^V" quoted-insert
bindkey "^X^B" vi-match-bracket
bindkey "^X^E" edit-command-line
bindkey "^X^F" vi-find-next-char
bindkey "^X^J" vi-join
bindkey "^X^K" kill-buffer
bindkey "^X^N" infer-next-history
bindkey "^X^O" overwrite-mode
bindkey "^X^R" _read_comp
bindkey "^X^U" undo
bindkey "^X^V" vi-cmd-mode
bindkey "^X^X" exchange-point-and-mark
bindkey "^X*" expand-word
bindkey "^X." fzf-tab-debug
bindkey "^X=" what-cursor-position
bindkey "^X?" _complete_debug
bindkey "^XC" _correct_filename
bindkey "^XG" list-expand
bindkey "^Xa" _expand_alias
bindkey "^Xc" _correct_word
bindkey "^Xd" _list_expansions
bindkey "^Xe" _expand_word
bindkey "^Xg" list-expand
bindkey "^Xh" _complete_help
bindkey "^Xm" _most_recent_file
bindkey "^Xn" _next_tags
bindkey "^Xr" history-incremental-search-backward
bindkey "^Xs" history-incremental-search-forward
bindkey "^Xt" _complete_tag
bindkey "^Xu" undo
bindkey "^X~" _bash_list-choices
bindkey "^Y" yank
bindkey "^[^D" list-choices
bindkey "^[^G" send-break
bindkey "^[^H" backward-kill-word
bindkey "^[^I" self-insert-unmeta
bindkey "^[^J" self-insert-unmeta
bindkey "^[^M" self-insert-unmeta
bindkey "^[^[" sudo-command-line
bindkey "^[^_" copy-prev-word
bindkey "^[ " expand-history
bindkey "^[!" expand-history
bindkey "^[\"" quote-region
bindkey "^[\$" spell-word
bindkey "^['" quote-line
bindkey "^[," _history-complete-newer
bindkey "^[-" neg-argument
bindkey "^[." insert-last-word
bindkey "^[/" _history-complete-older
bindkey "^[0" digit-argument
bindkey "^[1" digit-argument
bindkey "^[2" digit-argument
bindkey "^[3" digit-argument
bindkey "^[4" digit-argument
bindkey "^[5" digit-argument
bindkey "^[6" digit-argument
bindkey "^[7" digit-argument
bindkey "^[8" digit-argument
bindkey "^[9" digit-argument
bindkey "^[<" beginning-of-buffer-or-history
bindkey "^[>" end-of-buffer-or-history
bindkey "^[?" which-command
bindkey "^[A" accept-and-hold
bindkey "^[B" backward-word
bindkey "^[C" capitalize-word
bindkey "^[D" kill-word
bindkey "^[F" forward-word
bindkey "^[G" get-line
bindkey "^[H" run-help
bindkey "^[L" down-case-word
bindkey "^[N" history-search-forward
bindkey "^[OA" up-line-or-beginning-search
bindkey "^[OB" down-line-or-beginning-search
bindkey "^[OC" forward-char
bindkey "^[OD" backward-char
bindkey "^[OF" end-of-line
bindkey "^[OH" beginning-of-line
bindkey "^[P" history-search-backward
bindkey "^[Q" push-line
bindkey "^[S" spell-word
bindkey "^[T" transpose-words
bindkey "^[U" up-case-word
bindkey "^[W" copy-region-as-kill
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^[[200~" bracketed-paste
bindkey "^[[3;5~" kill-word
bindkey "^[[3~" delete-char
bindkey "^[[5~" up-line-or-history
bindkey "^[[6~" down-line-or-history
bindkey "^[[A" up-line-or-history
bindkey "^[[B" down-line-or-history
bindkey "^[[C" forward-char
bindkey "^[[D" backward-char
bindkey "^[[Z" reverse-menu-complete
bindkey "^[_" insert-last-word
bindkey "^[a" accept-and-hold
bindkey "^[b" backward-word
bindkey "^[c" capitalize-word
bindkey "^[d" kill-word
bindkey "^[f" forward-word
bindkey "^[g" get-line
bindkey "^[h" run-help
bindkey "^[m" copy-prev-shell-word
bindkey "^[n" history-search-forward
bindkey "^[p" history-search-backward
bindkey "^[q" push-line
bindkey "^[s" spell-word
bindkey "^[t" transpose-words
bindkey "^[u" up-case-word
bindkey "^[w" kill-region
bindkey "^[x" execute-named-cmd
bindkey "^[y" yank-pop
bindkey "^[z" execute-last-named-cmd
bindkey "^[|" vi-goto-column
bindkey "^[~" _bash_complete-word
bindkey "^[^?" backward-kill-word
bindkey " " magic-space
bindkey "!"-"~" self-insert
bindkey "^?" backward-delete-char
bindkey "\M-^@"-"\M-^?" self-insert
```
