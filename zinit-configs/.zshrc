
# 该zsh配置文件使用zinit进行插件管理

if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# 修复emacs中不能输入中文
# LC_CTYPE="zh_CN.utf8"

#  移除重复的命令历史
setopt HIST_IGNORE_ALL_DUPS

# 取消zsh在输出不以换行符结尾的内容是在其后添加百分号并另其一行的特性
# unsetopt prompt_cr prompt_sp

# 设置可以使用通配符
setopt nonomatch

# 免输入cd进入目录
setopt auto_cd

# 使用emacs的快捷键绑定
bindkey -v

# Go加速
export GO111MODULE=on
export GOPROXY=https://goproxy.cn,direct

# npm换源
npm config set registry http://registry.npm.taobao.org/

# 设置中文
export LANG=en_US.UTF-8
export LANGUAGE=en_US:en_US

# 不推荐使用，应该保留用于诊断调试用
# export LC_ALL=en_US.UTF-8

source ~/.zsh/fzf.zsh
source ~/.zsh/aliases.zsh
source ~/.zsh/env.zsh

# 加载zinit
if [[ ! -d ~/.zinit ]];then 
  git clone https://github.com/zdharma-continuum/zinit.git ~/.zinit/bin
fi
source ~/.zinit/bin/zinit.zsh


# 快速目录跳转
zinit ice lucid wait='1' # lucid ice 可以隐藏Turbo mode下插件加载完成的提示
zinit light skywind3000/z.lua

# 语法高亮
zinit ice lucid wait='0' atinit='zpcompinit'
zinit light zdharma-continuum/fast-syntax-highlighting

# 自动建议,C-f/Right应用建议
zinit ice lucid wait="0" atload='_zsh_autosuggest_start'
zinit light zsh-users/zsh-autosuggestions

# 补全
# zinit ice lucid wait='0'
# zinit light zsh-users/zsh-completions

# 根据子串搜索历史命令
zinit ice lucid wait="0"
zinit light zsh-users/zsh-autosuggestions


# 双击tab使用fzf补全
zinit light Aloxaf/fzf-tab
# disable sort when completing `git checkout`
zstyle ':completion:*:git-checkout:*' sort false
# set descriptions format to enable group support
zstyle ':completion:*:descriptions' format '[%d]'
# set list-colors to enable filename colorizing
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
# preview directory's content with exa when completing cd
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'exa -1 --color=always $realpath'
# switch group using `,` and `.`
zstyle ':fzf-tab:*' switch-group ',' '.'

# 加载 OMZ 框架及
zinit snippet OMZ::lib/completion.zsh
zinit snippet OMZ::lib/history.zsh

# 使用oh-my-zsh快捷键绑定
zinit snippet OMZ::lib/key-bindings.zsh

# 彩色man文档
zinit snippet OMZ::plugins/colored-man-pages/colored-man-pages.plugin.zsh

# 双击esc快速在命令前添加或删除sudo
zinit snippet OMZ::plugins/sudo/sudo.plugin.zsh

# vi模式
zinit snippet OMZ::plugins/vi-mode/vi-mode.plugin.zsh


zinit ice svn
zinit snippet OMZ::plugins/extract

zinit ice lucid wait='1'
zinit snippet OMZ::plugins/git/git.plugin.zsh


# 加载主题
zinit ice depth=1
zinit light romkatv/powerlevel10k

source ~/.zsh/fzf.zsh
source ~/.zsh/aliases.zsh
source ~/.zsh/keymaps.zsh

# 环境变量
export PATH="/home/feng/.local/bin:/home/feng/.luarocks/bin:$PATH"
export dirs="~/MyTools/resources/dir.list"
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# export PYENV_ROOT="$HOME/.pyenv"
# export PATH="$PYENV_ROOT/bin:~/.local/bin/:$PATH"
# if command -v pyenv 1>/dev/null 2>&1; then
#  eval "$(pyenv init -)"
# fi
# eval "$(pyenv virtualenv-init -)"

